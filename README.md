# create-react-webpack4
create-react-app 으로 생성한 webpack3.x 프로젝트를 webpack4로 커스터마이징 했다.

## 작업과정
### 프로젝트 생성
```
$ create-react-app j-react-config

$ cd j-react-config

// 기본 환경설정 추출
$ yarn eject

// 패키지 설치 및 업데이트
$ yarn
```

### 패키지 디펜던시 수정작업
```
// 먼저 webpack 최신화 
// 작성 당시 v4.8.1
$ yarn upgrade webpack@latest
```

**webpack 패키지 최신화 후 의존라이브러리들 경고**
![](create-react-webpack4/7912B56A-F43F-44B0-9F4E-7BA83AD6C1AF.png)

### 버전 맞춰주는 작업 시작
```
$ yarn upgrade webpack@latest
yarn add webpack-cli babel-loader@latest html-webpack-plugin@latest webpack-manifest-plugin@latest webpack-dev-server@latest webpack-dev-middleware@latest sw-precache-webpack-plugin@latest file-loader@latest ajv@latest eslint-loader@latest extract-text-webpack-plugin@next interpolate-html-plugin babel-polyfill cross-fetch lodash redux react-redux redux-saga react-hot-loader redux-devtools-extension react-loadable
```

 웹팩 설정 변경
구글링 결과 플러그인 순서에 영향이 있다고 한다.
아래 html plugin들의 순서를 변경해준다.

**webpack.config.dev.js**
``` javascript
// 상단에 import 구문 수정
// 새로 설치한 interpolate-html-plugin을 불러온다.
const InterpolateHtmlPlugin = require('interpolate-html-plugin');
...
...
...
// 플러그인 관련 부분 수정
plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.appHtml,
    }),
    new InterpolateHtmlPlugin(env.raw),

// 아래 주석처리
// new webpack.DefinePlugin(env.stringified),
// new WatchMissingNodeModulesPlugin(paths.appNodeModules),
...
...
...
],
// 아래 모드 추가
mode: 'development',
```

**webpack.config.prod.js**
``` javascript
// 상단에 import 구문 수정
// 새로 설치한 interpolate-html-plugin을 불러온다.
const InterpolateHtmlPlugin = require('interpolate-html-plugin');
...
...
...
// [contentshash:8] -> [hash:8]로 변경
const cssFilename = 'static/css/[name].[hash:8].css';
...
...
...
// 플러그인 관련 부분 수정
plugins: [
    new HtmlWebpackPlugin({
      inject: true,
      template: paths.appHtml,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        useShortDoctype: true,
        removeEmptyAttributes: true,
        removeStyleLinkTypeAttributes: true,
        keepClosingSlash: true,
        minifyJS: true,
        minifyCSS: true,
        minifyURLs: true,
      },
    }),
    new InterpolateHtmlPlugin(env.raw),

// 아래 주석처리
// new webpack.DefinePlugin(env.stringified),
/// new webpack.optimize.UglifyJsPlugin({/
///   compress: {/
///     warnings: false,/
///     // Disabled because of an issue with Uglify breaking seemingly valid code:/
///     // https://github.com/facebookincubator/create-react-app/issues/2376/
///     // Pending further investigation:/
///     // https://github.com/mishoo/UglifyJS2/issues/2011/
///     comparisons: false,/
///   },/
///   mangle: {/
///     safari10: true,/
///   },/
///   output: {/
///     comments: false,/
///     // Turned on because emoji and regex is not minified properly using default/
///     // https://github.com/facebookincubator/create-react-app/issues/2488/
///     ascii_only: true,/
///   },/
///   sourceMap: shouldUseSourceMap,/
/// }),/

...
...
...
],
// 아래 모드 추가
mode: 'production',
```

그리고

```
$ yarn start
```

![](create-react-webpack4/7F05BF94-16DF-42D1-958D-CC3C8D7839BD.png)

### 추가로 
babel-polyfill  --> config/polyfills.js에 임포트
cross-fetch --> config/polyfills.js 에 whatwg-fetch 대신 임포트
enzyme

등의 패키지 설치

#개발/project/j-react-config
